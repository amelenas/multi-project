package com.stepanovich.multiproject;

import java.util.ArrayList;

import static com.stepanovich.gradleproject.StringUtils.isPositiveNumber;

public class Utils {
    private Utils() {
    }

    public static boolean isAllPositiveNumbers(String... strings) {
        ArrayList<Boolean>result = new ArrayList<>();
        for (String string : strings) {
            result.add(isPositiveNumber(string));
        }
        return !result.contains(false);
    }

}
