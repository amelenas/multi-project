package com.stepanovich.multiproject;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class UtilsTest {

    @Test
    public void isAllPositiveNumbersPositive() {
        assertTrue(Utils.isAllPositiveNumbers("12", "79"));
    }

    @Test
    public void isAllPositiveNumbersNegative() {
        assertFalse(Utils.isAllPositiveNumbers("-12", "-79"));
    }

    @Test
    public void isAllPositiveNumbersPositiveAndNegative() {
        assertFalse(Utils.isAllPositiveNumbers("12", "-79"));
    }

    @Test
    public void isAllPositiveNumbersNegativeAndPositive() {
        assertFalse(Utils.isAllPositiveNumbers("-12", "79"));
    }
}